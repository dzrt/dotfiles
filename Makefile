stow:
	@stow git
	@stow vim
	@stow bash
	@stow npm

install.npm:
	@brew install npm
	@mkdir -p ${HOME}/.local/share/npm/bin
	@mkdir -p ${HOME}/.local/share/npm/cache

install.git:
	@brew install git

install.vim:
	@brew install vim

.PHONY: stow install.npm install.git install.vim

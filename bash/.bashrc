if [ -f ~/.bash_aliases ]; then
	. ~/.bash_aliases
fi

if [ -f ~/.bash_prompt ]; then
	. ~/.bash_prompt
fi

export EDITOR=vim
source "$HOME/.vim/gruvbox_256palette.sh"

# paths
export PATH="$HOME/.local/bin:/usr/local/bin:$PATH"

# npm
if [ -f ~/.npmrc ]; then
	export PATH="$HOME/.local/share/npm/bin:$PATH"
fi

# go
export PATH="$HOME/go/bin:$PATH"
if [ -f ~/.go/env ]; then
	. ~/.go/env
fi

# rust
if [ -f ~/.cargo/env ]; then
	. ~/.cargo/env
fi

# put updated vim first in $PATH
export PATH="/usr/local/Cellar/vim/8.1.2250/bin:$PATH"


source /Users/dzrt/Library/Preferences/org.dystroy.broot/launcher/bash/br

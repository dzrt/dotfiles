call plug#begin()
Plug 'tpope/vim-sensible'
Plug 'morhetz/gruvbox'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'easymotion/vim-easymotion'
Plug 'scrooloose/nerdtree'
Plug 'rust-lang/rust.vim'
"Plug 'vim-syntastic/syntastic'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'editorconfig/editorconfig-vim'
call plug#end()

let mapleader=","

syntax on
filetype on

let g:gruvbox_italic=1
let g:gruvbox_italicize_comments=1

colorscheme gruvbox
set termguicolors

" airline
let g:airline_theme='minimalist'

" ctrlp (use buffers!)
" setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

" Ignore files in .gitignore, .hgignore, .agignore!
let g:ctrlp_user_command = {
  \ 'types': {
    \ 1: ['.git', 'git -C %s ls-files --cached --exclude-standard --others'],
    \ 2: ['.hg', 'hg --cwd %s locate -I .'],
    \ },
  \ 'fallback': 'ag %s -l --nocolor --hidden -g ""'
  \ }

" Use a leader instead of the actual named binding
"nmap <leader>p :CtrlP<cr>
"nmap <leader>P :CtrlPMRU<cr>

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bx :CtrlPMixed<cr>
nmap <leader>bn :CtrlPMRU<cr>

" rust.vim: binds
nmap <leader>rr :RustRun<cr>
nmap <leader>ff :RustFmt<cr>

" show commands in lower right
set showcmd

" copy/paste from system clipboard
nnoremap <leader>y "+y
nnoremap <leader>p "+p

set tabstop=4
set shiftwidth=4
set splitbelow
set splitright
set number
hi VertSplit ctermbg=NONE guibg=NONE

" jump to beginning and end of line
inoremap <C-e> <C-o>$
inoremap <C-a> <C-o>0

" (normal mode) cycle windows
nnoremap <tab> <C-w><C-w>

" (normal mode) toggle NERDTree
nnoremap <C-n> :NERDTreeToggle<CR>

" EasyMotion
nmap s <Plug>(easymotion-s2)
nmap t <Plug>(easymotion-t2)

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Rust
let g:rustfmt_autosave=1

